<!docAgama html>
<html>
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.css">
        

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-vertical-tabs/bootstrap.vertical-tabs.css">     

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">

        
        <!-- jquery js -->
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.js"></script>
    
        <!-- bootstrap js -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.js"></script>

        <!-- bootstrap datepicker js -->
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">      

    </head>
    <body>

     <div class="container white-container">

        <h2 style="margin-top:10px">Masuka data diri anda <!-- <?php echo $button ?> --></h2>
        </br>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">

	    <div class="form-group">
            <label for="varchar" class="col-md-4 control-label required">Nama <?php echo form_error('nama') ?></label>
            <div class="col-md-4">
            <input Nama="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
            </div>
        </div>

         <div class="form-group">
            <label for="timestamp" class="col-md-4 control-label required">Tanggal Lahir <?php echo form_error('tanggal_lahir') ?></label>
            <div class="col-md-4">
            <input type="text" class="form-control datepicker" aria-label="Tanggal lahir"  name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" value="<?php echo $tanggal_lahir; ?>" required="required" />
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>

	    <div class="form-group">
            <label for="int" class="col-md-4 control-label required">Agama</label>

             <div class="col-md-4">
             <select class="form-control" name="agama" id="agamas">
                    <option value="">Pilih Agama</option>
                    <?php foreach ($agamas as $agama): ?>
                        
                        <option value="<?= $agama->id ?>"><?php echo $agama->agama ?></option>
                    
                    <?php endforeach ?>

            </select>
           
            </div>
        </div>

	    <div class="form-group">
            <label for="int" class="col-md-4 control-label required">pendidikan</label>

             <div class="col-md-4">
             <select class="form-control" name="pendidikans" id="pendidikans">
                     <option value="">Pilih pendidikan</option>
                    <?php foreach ($pendidikans as $pendidikan) {
                   ?>
                    <option                                 
                    value="<?php echo $pendidikan->id ?>"><?php echo $pendidikan->pendidikan ?></option>
                    <?php  } ?>  

            </select>
          
            </div>
        </div>


	    <div class="form-group">
            <label for="int" class="col-md-4 control-label required">Tipe Badan</label>

             <div class="col-md-4">
             <select class="form-control" name="tipe_badans" id="tipe_badans">
                     <option value="">Pilih tipe badan</option>
                    <?php foreach ($tipe_badans as $tipe_badan) {
                   ?>
                    <option                                 
                    value="<?php echo $tipe_badan->id?>"><?php echo $tipe_badan->tipe ?></option>
                    <?php  } ?>  

            </select>
            </div>
        </div>

        <div class="form-group">
            <label for="varchar" class="col-md-4 control-label required">Tinggi Badan <?php echo form_error('tinggi') ?></label>
            <div class="col-md-4">
            <input tinggi="text" class="form-control" name="tinggi" id="tinggi" placeholder="Tinggi Badan" value="<?php echo $tinggi; ?>" />
            </div>
        </div>


	    <div class="form-group">
            <label for="int" class="col-md-4 control-label required">Penghasilan</label>

             <div class="col-md-4">
             <select class="form-control" name="penghasilans" id="penghasilans">
                     <option value="">Pilih pendidikan</option>
                    <?php foreach ($penghasilans as $penghasilan) {
                   ?>
                    <option                                 
                    value="<?php echo $penghasilan->id ?>"><?php echo $penghasilan->penghasilan ?></option>
                    <?php  } ?>  

            </select>
          
            </div>
        </div>


         <div class="form-group">
            <label for="int" class="col-md-4 control-label required">Apakah anda merokok?</label>

             <div class="col-md-4">
        
                <input type="radio" name="merokok" value="YA" checked> YA<br>
                <input type="radio" name="merokok" value="tidak"> TIDAK<br>
          
            </div>
        </div>


	    <div class="form-group">
            <label for="varchar" class="col-md-4 control-label required">Ceritakan tentang diri anda<?php echo form_error('about_me') ?></label>
            <div class="col-md-4">
            <textarea class="form-control" rows="5" id="about_me" name="about_me" placeholder="Ceritakan tentang dirimu"></textarea>
            
            </div>
        </div>
	

	    <input type="hidden" name="id_pengguna" value="<?php echo $id_pengguna; ?>" /> 
       <div class="save-cancel well text-right">
                <input type="submit" class="btn btn-success submit-btn btn-md" value="Tambah" />
                &nbsp;
                <a href="<?php echo site_url('pengguna') ?>" class="btn btn-default">Batal</a>
            </div>              
        </div>
    </div>
	</form>
    </body>
</html>