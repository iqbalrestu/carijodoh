<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.css">


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-vertical-tabs/bootstrap.vertical-tabs.css">     

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">


    <!-- jquery js -->
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.js"></script>
    
    <!-- bootstrap js -->
    <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.js"></script>

    <!-- bootstrap datepicker js -->
    <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">      

</head>
<body>

   <div class="container white-container">

    <h2 style="margin-top:10px">Jawab pertanyaan berikut <!-- <?php echo $button ?> --></h2>
</br>
<!--   <form action="<?php echo $action; ?>" method="post" class="form-horizontal"> -->

<form action="pertanyaan/hasil" method="post">

<?php foreach ($data as $pertanyaan): ?>
    <div class="form-group">
    <label for="int" class="col-md-6 control-label required"><?php echo $pertanyaan->isi_pertanyaan ?></label>
    <div class="col-md-6">
        <?php 
        switch ($pertanyaan->tipe) {
            case "agama":
            ?>
            <select name="agama" class="form-control" >
                <option value="1">Penting</option>
                <option value="0">Tidak Penting</option>
            </select>
            <?php
            break;
            case "umur":
            ?>
            <select name="umur" class="form-control" >
                <option value="0">> Muda</option>
                <option value="1">Sama</option>
                <option value="2">> Tua</option>
                <option value="3">Tidak Penting</option>
            </select>
            <?php
            break;
            case "hubungan":
            ?>
            <select name="hubungan" class="form-control" >
                <option value="1">Serius</option>
                <option value="0">Hanya berteman</option>
            </select>
            <?php
            break;
            case "merokok":
            ?>
            <select name="merokok" class="form-control" >
                <option value="1">YA</option>
                <option value="0">TIDAK</option>
            </select>
            <?php
            break;
            case "pendidikan":
            ?>
            <select name="pendidikan" class="form-control" >
                <option value="0">< S1 </option>
                <option value="1">S1</option>
                <option value="2"> >S1</option>

            </select>
            <?php
            break;
            case "tinggi":
            ?>
            <select name="tinggi" class="form-control" >
                <option value="0">lebih tingi</option>
                <option value="1">setara</option>
                <option value="2">tidak penting</option>
            </select>
            <?php
            break;
            case "badan":
            ?>
            <select name="badan" class="form-control" >
                <option value="1">ideal</option>
                <option value="2">kurus</option>
                <option value="3">agak gemuk</option>
                <option value="4">gemuk</option>
            </select>
            <?php
            break;
            case "penampilan":
            ?>
            <select name="penampilan" class="form-control" >
                <option value="1">penting</option>
                <option value="0">tidak penting</option>
            </select>
            <?php
            break;
            case "sifat":
            ?>
            <select name="sifat" class="form-control" >
                <option value="0">mudah emosi</option>
                <option value="1">merasa lebih sabar</option>
            </select>
            <?php
            break;
            case "penghasilan":
            ?>
            <select name="penghasilan" class="form-control" >
                <option value="0">0 - 2 JT</option>
                <option value="1">2 - 5 JT</option>
                <option value="2">5 - 10 JT</option>
                <option value="3"> > 10 JT</option>
            </select>
            <?php
            break;
            default:
            echo "Your favorite color is neither red, blue, nor green!";
        }
        ?>
    </div>
</div>
<?php endforeach ?>

<button type="submit">Lihat Jodohmu</button>



</form>
</body>
</html>