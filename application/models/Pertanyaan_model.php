<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pertanyaan_model extends CI_Model
{

    public $table = 'pertanyaan';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    public function getpertanyaan()
    {
        $query = $this->db->query('SELECT id_pertanyaan, isi_pertanyaan, tipe FROM pertanyaan');
        return $query->result();
    }

    public function getagamadropdown(){
        $query = $this->db->query('SELECT id, agama FROM agama');
           return $query->result();
        }

   }
   ?>