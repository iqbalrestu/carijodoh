<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hasil extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kriteria_model');
        $this->load->model('Pengguna_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
       $kriteria = $this->Kriteria_model->getLast();
       $pengguna = $this->Pengguna_model->getLast();

       $where = 'WHERE ';
       $temp = '';
       $agama = $kriteria[0]->agama;
       $tipe_badan = $kriteria[0]->tipe_badan;
       $umur = $kriteria[0]->umur;
       $merokok = $kriteria[0]->merokok;
       $pendidikan = $kriteria[0]->pendidikan;
       $tinggi = $kriteria[0]->tinggi;
       $tipe_hubungan = $kriteria[0]->tipe_hubungan;
       $penghasilan = $kriteria[0]->penghasilan;

       if ($agama == 1) {
           $temp = $temp.'agama = '.$pengguna[0]->agama.' AND ';
       }

       $temp = $temp.'merokok = '.$merokok.' AND ';
       $temp = $temp.'pendidikan = '.$pendidikan.' AND ';
       $temp = $temp.'penghasilan = '.$penghasilan;
       $where = $where.$temp;

       $data = $this->Pengguna_model->getAllWhere($where);

       $this->load->view('hasil', array('data' => $data ));
    }

    public function read($id) 
    {
       
    }

    
}

