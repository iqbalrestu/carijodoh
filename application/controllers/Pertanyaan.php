<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pertanyaan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pertanyaan_model');
        $this->load->model('Kriteria_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data = $this->Pertanyaan_model->getpertanyaan();
        $this->load->view('inc/header');
        $this->load->view('pertanyaan/kriteria', array('data' => $data));
        $this->load->view('inc/footer');
    }

    public function hasil()
    {
        $data = array(
            'agama' => $this->input->post('agama'),
            'tipe_hubungan' => $this->input->post('hubungan'),
            'merokok' => $this->input->post('merokok'),
            'pendidikan' => $this->input->post('pendidikan'),
            'tinggi' => $this->input->post('tinggi'),
            'tipe_badan' => $this->input->post('badan'),
            'penghasilan' => $this->input->post('penghasilan')    
        );
        $this->Kriteria_model->insert($data);
        $this->session->set_flashdata('message', 'Create Record Success');
        redirect('hasil');
    }
}