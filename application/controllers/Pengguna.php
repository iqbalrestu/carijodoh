<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pengguna_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pengguna/.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pengguna/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pengguna/index.html';
            $config['first_url'] = base_url() . 'pengguna/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pengguna_model->total_rows($q);
        $pengguna = $this->Pengguna_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pengguna_data' => $pengguna,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('pengguna/pertanyaan', $data);
    }

    public function read($id) 
    {
        $row = $this->Pengguna_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_pengguna' => $row->id_pengguna,
		'nama' => $row->nama,
		'tanggal_lahir' => $row->tanggal_lahir,
		'agama' => $row->agama,
		'pendidikan' => $row->pendidikan,
		'tipe_badan' => $row->tipe_badan,
		'penghasilan' => $row->penghasilan,
		'about_me' => $row->about_me,
		'merokok' => $row->merokok,
	    );
            $this->load->view('pengguna/pengguna_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function create() 
    {
        $this->load->model('Pengguna_model');
        $this->load->helper('form_helper');
        $agamas =$this->data['agama']= $this->Pengguna_model->getagamadropdown();
        $pendidikans =$this->Pengguna_model->getpendidikandropdown();
        $tipe_badans =$this->Pengguna_model->gettipe_badandropdown();
        $penghasilans =$this->Pengguna_model->getpenghasilandropdown();
        

        // print_r($agamas);
        // print_r($pendidikans);
        // print_r($tipe_badans);
        // print_r($penghasilans);
        
        $data = array(
            'button' => 'Create',
            'action' => site_url('pengguna/create_action'),
            'id_pengguna' => set_value('id_pengguna'),
            'nama' => set_value('nama'),
            'tanggal_lahir' => set_value('tanggal_lahir'),
            'agama' => set_value('agama'),
            'pendidikan' => set_value('pendidikan'),
            'tipe_badan' => set_value('tipe_badan'),
            'tinggi' => set_value('tinggi'),
            'penghasilan' => set_value('penghasilan'),
            'about_me' => set_value('about_me'),
            'merokok' => set_value('merokok'),
            'agamas' => $agamas,
            'pendidikans' => $pendidikans,
            'tipe_badans' => $tipe_badans,
            'penghasilans' => $penghasilans

        ); 

        // print_r($data);
        // die();

        $this->load->view('inc/header');
        $this->load->view('pengguna/pengguna_form', $data);
        $this->load->view('inc/footer');
    }
    
    public function create_action() 
    {
        $date = date("Y-m-d",strtotime($this->input->post('start_date')));


        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'tanggal_lahir' => $this->input->post('tanggal_lahir',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'pendidikan' => $this->input->post('pendidikans',TRUE),
		'tipe_badan' => $this->input->post('tipe_badans',TRUE),
        'tinggi' => $this->input->post('tinggi',TRUE),
		'penghasilan' => $this->input->post('penghasilans',TRUE),
		'about_me' => $this->input->post('about_me',TRUE),
		'merokok' => $this->input->post('merokok',TRUE),
        'tanggal_lahir' => $date
	    );
        //     print_r($data);
        // die();
        

            $this->Pengguna_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pengguna'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pengguna_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pengguna/update_action'),
		'id_pengguna' => set_value('id_pengguna', $row->id_pengguna),
		'nama' => set_value('nama', $row->nama),
		'tanggal_lahir' => set_value('tanggal_lahir', $row->tanggal_lahir),
		'agama' => set_value('agama', $row->agama),
		'pendidikan' => set_value('pendidikan', $row->pendidikan),
		'tipe_badan' => set_value('tipe_badan', $row->tipe_badan),
		'penghasilan' => set_value('penghasilan', $row->penghasilan),
		'about_me' => set_value('about_me', $row->about_me),
		'merokok' => set_value('merokok', $row->merokok),
	    );
            $this->load->view('pengguna/pengguna_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pengguna', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'tanggal_lahir' => $this->input->post('tanggal_lahir',TRUE),
		'agama' => $this->input->post('agama',TRUE),
		'pendidikan' => $this->input->post('pendidikan',TRUE),
		'tipe_badan' => $this->input->post('tipe_badan',TRUE),
		'penghasilan' => $this->input->post('penghasilan',TRUE),
		'about_me' => $this->input->post('about_me',TRUE),
		'merokok' => $this->input->post('merokok',TRUE),
	    );

            $this->Pengguna_model->update($this->input->post('id_pengguna', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pengguna'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pengguna_model->get_by_id($id);

        if ($row) {
            $this->Pengguna_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pengguna'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('tanggal_lahir', 'tanggal lahir', 'trim|required');
	// $this->form_validation->set_rules('agama', 'agama', 'trim|required');
	// $this->form_validation->set_rules('pendidikan', 'pendidikan', 'trim|required');
	// $this->form_validation->set_rules('tipe_badan', 'tipe badan', 'trim|required');
	// $this->form_validation->set_rules('penghasilan', 'penghasilan', 'trim|required');
	// $this->form_validation->set_rules('about_me', 'about me', 'trim|required');
	// $this->form_validation->set_rules('merokok', 'merokok', 'trim|required');

	$this->form_validation->set_rules('id_pengguna', 'id_pengguna', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-27 21:25:37 */
/* http://harviacode.com */