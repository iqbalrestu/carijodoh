/*
Navicat MySQL Data Transfer

Source Server         : LOCAL
Source Server Version : 100122
Source Host           : localhost:3306
Source Database       : jodoh

Target Server Type    : MYSQL
Target Server Version : 100122
File Encoding         : 65001

Date: 2017-07-28 13:51:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agama
-- ----------------------------
DROP TABLE IF EXISTS `agama`;
CREATE TABLE `agama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agama
-- ----------------------------
INSERT INTO `agama` VALUES ('1', 'Islam');
INSERT INTO `agama` VALUES ('2', 'Hindu');
INSERT INTO `agama` VALUES ('3', 'Budha');
INSERT INTO `agama` VALUES ('4', 'Kristen');

-- ----------------------------
-- Table structure for kriteria
-- ----------------------------
DROP TABLE IF EXISTS `kriteria`;
CREATE TABLE `kriteria` (
  `id_kriteria` int(5) NOT NULL AUTO_INCREMENT,
  `agama` int(25) NOT NULL,
  `tipe_badan` int(25) NOT NULL,
  `umur` int(3) NOT NULL,
  `merokok` tinyint(1) NOT NULL,
  `pendidikan` int(10) NOT NULL,
  `tinggi` int(10) NOT NULL,
  `tipe_hubungan` int(25) NOT NULL,
  `penghasilan` int(25) NOT NULL,
  `id_pengguna` int(11) NOT NULL,
  PRIMARY KEY (`id_kriteria`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kriteria
-- ----------------------------

-- ----------------------------
-- Table structure for pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `pendidikan`;
CREATE TABLE `pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pendidikan
-- ----------------------------
INSERT INTO `pendidikan` VALUES ('1', '< S1');
INSERT INTO `pendidikan` VALUES ('2', 'S1');
INSERT INTO `pendidikan` VALUES ('3', '> S1');

-- ----------------------------
-- Table structure for pengguna
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` int(10) NOT NULL,
  `pendidikan` int(10) NOT NULL,
  `tipe_badan` int(10) NOT NULL,
  `tinggi` int(10) NOT NULL,
  `penghasilan` int(10) NOT NULL,
  `about_me` varchar(50) NOT NULL,
  `merokok` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_pengguna`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengguna
-- ----------------------------
INSERT INTO `pengguna` VALUES ('1', 'Iqbal', '0000-00-00', '0', '0', '0', '0', '25', 'ini about me', '0');

-- ----------------------------
-- Table structure for penghasilan
-- ----------------------------
DROP TABLE IF EXISTS `penghasilan`;
CREATE TABLE `penghasilan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penghasilan` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of penghasilan
-- ----------------------------
INSERT INTO `penghasilan` VALUES ('1', '0 - 2 JT');
INSERT INTO `penghasilan` VALUES ('2', '2 - 5 JT');
INSERT INTO `penghasilan` VALUES ('3', '5 - 10 JT');
INSERT INTO `penghasilan` VALUES ('4', '> 10 JT');

-- ----------------------------
-- Table structure for pertanyaan
-- ----------------------------
DROP TABLE IF EXISTS `pertanyaan`;
CREATE TABLE `pertanyaan` (
  `id_pertanyaan` int(11) NOT NULL,
  `isi_pertanyaan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pertanyaan
-- ----------------------------
INSERT INTO `pertanyaan` VALUES ('1', 'apakah kamu beragama islam?');
INSERT INTO `pertanyaan` VALUES ('2', 'berapa umur jodoh yang anda cari?');
INSERT INTO `pertanyaan` VALUES ('3', 'Apakah tipe hubungan yang diinginkan?');
INSERT INTO `pertanyaan` VALUES ('4', 'apakah kamu merokok?');
INSERT INTO `pertanyaan` VALUES ('5', 'apakah pendidikan terakhir kamu?');
INSERT INTO `pertanyaan` VALUES ('6', 'berapakah tinggi badan kamu?');
INSERT INTO `pertanyaan` VALUES ('7', 'Apakah tipe badan kamu?');
INSERT INTO `pertanyaan` VALUES ('8', 'apakah penampilan fisik penting bagi anda?');
INSERT INTO `pertanyaan` VALUES ('9', 'menurut anda apakah sifat anda?');
INSERT INTO `pertanyaan` VALUES ('10', 'seberapa penting uang dalam hubungan anda?');

-- ----------------------------
-- Table structure for tipe_badan
-- ----------------------------
DROP TABLE IF EXISTS `tipe_badan`;
CREATE TABLE `tipe_badan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipe_badan
-- ----------------------------
INSERT INTO `tipe_badan` VALUES ('1', 'Kurus');
INSERT INTO `tipe_badan` VALUES ('2', 'Ideal');
INSERT INTO `tipe_badan` VALUES ('3', 'Agak Gemuk');
INSERT INTO `tipe_badan` VALUES ('4', 'Gemuk');

-- ----------------------------
-- Table structure for tipe_hubungan
-- ----------------------------
DROP TABLE IF EXISTS `tipe_hubungan`;
CREATE TABLE `tipe_hubungan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipe_hubungan
-- ----------------------------
